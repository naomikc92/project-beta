import React, { useEffect, useState } from 'react';

function TechnicianList() {
    const [technicians, setTechnicians] = useState(null);

    useEffect(() => {
        fetchTechnicians();
    }, []);

    const fetchTechnicians = () => {
        fetch('http://localhost:8080/api/technicians/')
            .then(response => response.json())
            .then(data => {
                if (data && data.techs) {
                    setTechnicians(data.techs);
                } else {
                    console.error('data structure not loading right, dingus:', data);
                }
            })
            .catch(error => console.error('Error fetching technicians not working, dingus:', error));
    };

    const handleDelete = (technicianID) => {
        const technicianUrl = `http://localhost:8080/api/technicians/${technicianID}/`;

        fetch(technicianUrl, {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json',
            },
        })
        .then(response => {
            if (response.ok) {
                console.log("Technician deleted.");
                fetchTechnicians(); 
            }
        });
    }

    return (
        <div>
            <h2>List of Technicians</h2>
            {technicians == null ? (
                <p>Loading technicians...</p>
            ) : technicians.length === 0 ? (
                <p>No technicians found.</p>
            ) : (
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Employee ID</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {technicians.map(technician => (
                            <tr key={technician.id}>
                                <td>{technician.employee_id}</td>
                                <td>{technician.first_name}</td>
                                <td>{technician.last_name}</td>
                                <td>
                                    <button onClick={() => handleDelete(technician.id)}>Delete</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            )}
        </div>
    );
}

export default TechnicianList;

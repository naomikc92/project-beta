import React, { useState } from "react";

const ManufacturerForm = () => {
    const [ name, setName ] = useState('');


    const handleNameChange = (event) => {
      const { value } = event.target;
      setName(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = { name };

        const url = "http://localhost:8100/api/manufacturers/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newManufacturer = await response.json();
            console.log(newManufacturer)
            setName('');
        }

    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a manufacturer</h1>
              <form onSubmit={handleSubmit} id="create-presentation-form">
                <div className="form-floating mb-3">
                  <input onChange={handleNameChange} value={name} placeholder="Manufacturer name..." required type="text" id="name" className="form-control" />
                  <label htmlFor="title">Name</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    
}

export default ManufacturerForm;
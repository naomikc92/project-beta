import React, {useState} from "react";


const CreateSalespersonForm = () => {
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('')
    const [address, setAddress] = useState('')
    const [phone_number, setPhoneNumber] = useState('')


    const handleFirstNameChange = (event) => {
        const { value } = event.target;
        setFirstName(value)
    }

    const handleLastNameChange = (event) => {
        const { value } = event.target;
        setLastName(value)
    }

    const handleAddressChange = (event) => {
        const { value } = event.target;
        setAddress(value)
    }

    const handlePhoneNumberChange = (event) => {
        const { value } = event.target;
        setPhoneNumber(value)
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            first_name, 
            last_name,
            address,
            phone_number
        };

        const url = "http://localhost:8090/api/customers/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();
            console.log(newCustomer)
            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNumber('');
        }

    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a Customer</h1>
              <form onSubmit={handleSubmit} id="create-presentation-form">
                <div className="form-floating mb-3">
                  <input onChange={handleFirstNameChange} value={first_name} placeholder="first name" required type="text" id="firstName" className="form-control" />
                  <label htmlFor="first name">First Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleLastNameChange} value={last_name} placeholder="last name" required type="text" id="lastName" className="form-control" />
                  <label htmlFor="last name">Last Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleAddressChange} value={address} placeholder="address" required type="text" id="Address" className="form-control" />
                  <label htmlFor="address">Address</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handlePhoneNumberChange} value={phone_number} placeholder="phone number" required type="text" id="phoneNumber" className="form-control" />
                  <label htmlFor="phone number">Phone #</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
}

export default CreateSalespersonForm;
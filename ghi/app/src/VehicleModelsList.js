import React from 'react';

function ListVehicleModels(props) {
    if (props.models === undefined) {
        return null;
    }

    const handleDelete = (modelID) => {
        const modelUrl = `http://localhost:8100/api/models/${modelID}/`;
        console.log(modelUrl);

        fetch(modelUrl, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },
        })
        .then(response => {
            if (response.ok) {
                console.log("Model deleted.");
                window.location.reload();
            }
        });
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Model</th>
                    <th>Picture</th>
                    <th>Manufacturer</th>
                    {/* <th>Action</th> //delete button? */}
                </tr>
            </thead>
            <tbody>
                {props.models.map(model => {
                    return (
                        <tr key={model.href}>
                            <td>{ model.name }</td>
                            <td>
                                <img src={model.picture_url} alt={model.name} />
                            </td>
                            <td>{ model.manufacturer.name }</td>
                            <td>
                                <button onClick={() => handleDelete(model.id)}>Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    );
}

export default ListVehicleModels;

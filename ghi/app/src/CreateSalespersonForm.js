import React, {useState} from "react";


const CreateSalespersonForm = () => {
    const [employee_id, setEmployeeID] = useState('')
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('')


    const handleFirstNameChange = (event) => {
        const { value } = event.target;
        setFirstName(value)
    }

    const handleLastNameChange = (event) => {
        const { value } = event.target;
        setLastName(value)
    }

    const handleEmployeeIDChange = (event) => {
        const { value } = event.target;
        setEmployeeID(value)
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            employee_id, 
            first_name, 
            last_name
        };

        const url = "http://localhost:8090/api/salespeople/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newSalesperson = await response.json();
            console.log(newSalesperson)
            setEmployeeID('');
            setFirstName('');
            setLastName('');
        }

    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add a Salesperson</h1>
              <form onSubmit={handleSubmit} id="create-presentation-form">
                <div className="form-floating mb-3">
                  <input onChange={handleEmployeeIDChange} value={employee_id} placeholder="employee ID" required type="text" id="employeeID" className="form-control" />
                  <label htmlFor="employee id">Employee ID</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleFirstNameChange} value={first_name} placeholder="first name" required type="text" id="firstName" className="form-control" />
                  <label htmlFor="first name">First Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handleLastNameChange} value={last_name} placeholder="last name" required type="text" id="lastName" className="form-control" />
                  <label htmlFor="last name">Last Name</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
}

export default CreateSalespersonForm;
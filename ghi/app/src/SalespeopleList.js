import React from "react";


const SalespeopleList = (props) => {
    return (
        <div>
            <h1>Salespeople</h1>
            <table className="salepeople-table table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {props.salespeople.map( person => {
                        return (
                            <tr key={person.id}>
                                <td>{person.employee_id}</td>
                                <td>{person.first_name}</td>
                                <td>{person.last_name}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}


export default SalespeopleList
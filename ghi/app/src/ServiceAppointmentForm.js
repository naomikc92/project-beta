import React, { useState, useEffect } from "react";

const ServiceAppointmentForm = () => {
  const [vin, setVin] = useState('');
  const [customer, setCustomer] = useState('');
  const [dateTime, setDateTime] = useState('');
  const [technicianId, setTechnicianId] = useState('');
  const [reason, setReason] = useState('');
  const [status, setStatus] = useState('on hold'); // Default status
  const [technicians, setTechnicians] = useState([]);

  useEffect(() => {
    fetch('http://localhost:8080/api/technicians/')
      .then(response => response.json())
      .then(data => setTechnicians(data.techs));
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      vin,
      customer,
      date_time: dateTime,
      technician: parseInt(technicianId),
      reason,
      status,
    };
console.log("handle submit", data)
    const url = "http://localhost:8080/api/appointments/"
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      const newAppointment = await response.json();
      console.log(newAppointment);
      setVin('');
      setCustomer('');
      setDateTime('');
      setTechnicianId('');
      setReason('');
      setStatus('');
    }
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a Service Appointment</h1>
          <form onSubmit={handleSubmit} id="create-appointment-form">
            <div className="form-floating mb-3">
              <input value={vin} onChange={e => setVin(e.target.value)} placeholder="Vehicle VIN..." required type="text" id="vin" className="form-control" />
              <label htmlFor="vin">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input value={customer} onChange={e => setCustomer(e.target.value)} placeholder="Customer name..." required type="text" id="customer" className="form-control" />
              <label htmlFor="customer">Customer</label>
            </div>
            <div className="form-floating mb-3">
              <input value={dateTime} onChange={e => setDateTime(e.target.value)} placeholder="Date and Time..." required type="datetime-local" id="dateTime" className="form-control" />
              <label htmlFor="dateTime">Date & Time</label>
            </div>
            <div className="form-floating mb-3">
              <select
                value={technicianId}
                onChange={(e) => setTechnicianId(e.target.value)}
                required
                className="form-control"
              >
                <option value="">Choose a technician</option>
                {technicians.map((technician) => (
                  <option key={technician.id} value={technician.id}>
                    {technician.first_name} {technician.last_name}
                  </option>
                ))}
              </select>
              <label htmlFor="technician">Technician</label>
            </div>
            <div className="form-floating mb-3">
              <input value={reason} onChange={e => setReason(e.target.value)} placeholder="Reason for appointment..." required type="text" id="reason" className="form-control" />
              <label htmlFor="reason">Reason</label>
            </div>
            {/* Status Dropdown */}
            <div className="form-floating mb-3">
              <select
                value={status}
                onChange={(e) => setStatus(e.target.value)}
                required
                className="form-control"
              >
                <option value="Canceled">Canceled</option>
                <option value="On Hold">On Hold</option>
                <option value="In Progress">In Progress</option>
                <option value="Finished">Finished</option>
              </select>
              <label htmlFor="Status">Status</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ServiceAppointmentForm;

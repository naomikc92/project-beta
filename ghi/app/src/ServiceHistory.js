import React, { useState, useEffect } from "react";

const ServiceHistoryList = () => {
  const [serviceHistory, setServiceHistory] = useState([]);
  const [vin, setVin] = useState('');

  const fetchServiceHistory = async () => {
    try {
      const response = await fetch(`http://localhost:8080/api/service-history/${vin}/`);
      if (response.ok) {
        const data = await response.json();
        setServiceHistory(data.service_history);
      } else {
        console.error("Error because you are a dingus");
      }
    } catch (error) {
      console.error("Fetch not working:", error);
    }
  };

  const handleDelete = async (serviceId) => {
    try {
      const response = await fetch(`http://localhost:8080/api/service-history/${serviceId}/`, {
        method: "delete",
        headers: {
          "Content-Type": "application/json",
        },
      });

      if (response.ok) {
        console.log("Service history deleted!");
        fetchServiceHistory(); 
      } else {
        console.error("Error in service history");
      }
    } catch (error) {
      console.error("fetch not working:", error);
    }
  };

// something in the handle submit is causing that console log, come back to and fix it

  const handleSubmit = (event) => {
    event.preventDefault();
    fetchServiceHistory();
  }

  useEffect(() => {
    fetchServiceHistory();
  }, []);

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <input 
          type="text" 
          placeholder="Enter VIN"
          value={vin}
          onChange={(e) => setVin(e.target.value)}
        />
        <button type="submit">Submit</button>
      </form>

      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer Name</th>
            <th>Date/Time</th>
            <th>Technician ID</th>
            <th>Service Reason</th>
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {serviceHistory.map(service => (
            <tr key={service.id}>
              <td>{service.vin}</td>
              <td>{service.is_vip ? 'Yes' : 'No'}</td>
              <td>{service.customer}</td>
              <td>{service.date_time}</td>
              <td>{service.technician_id}</td>
              <td>{service.reason}</td>
              <td>{service.status}</td>
              <td>
                <button onClick={() => handleDelete(service.id)}>Delete</button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default ServiceHistoryList;



#  I've created these in a seperate file because that is how it was done in the inventory file and it seems sensible.
#  Remember to import/export this file correctly, these might take care of the 400 errors. 
#   note, if these are not properly configured, insomnia show attribute errors like 


from common.json import ModelEncoder
from .models import AutomobileVO, Tech, Appointment


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        'id',
        'vin',
        'sold',
    ]


class TechEncoder(ModelEncoder):
    model = Tech
    properties = [
        'id',
        'first_name',
        'last_name',
        'employee_id',
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        'id',
        'date_time',
        'reason',
        'status',
        'vin',
        'customer',
        'technician',
    ]
    encoders = {
        'technician': TechEncoder(),
    }



# the first one takes my automobile value object and turns it into a dictionary with the keys "id, vin, sold"

# the second one converts tech into a dictionsry with the keys "id, first_name, last_name and Employee_id"

#the third one converts blargblah to blah whatever I am very tired
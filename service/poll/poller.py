import json
import django
import os
import sys
import time
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

from service_rest import models
from service_rest.models import AutomobileVO

INVENTORY_API_URL = "http://project-beta-inventory-api-1:8000/api/automobiles/" # ok this was silly but why not

def get_automobiles():
    response = requests.get(INVENTORY_API_URL)
    content = json.loads(response.content)
    
    for automobile in content["autos"]:
            AutomobileVO.objects.update_or_create(
                 vin=automobile["vin"],
                 defaults={"sold": automobile["sold"]}
            )
  

def poll():
    while True:
        print('Automobile service poller polling for data')
        try:
            get_automobiles()

        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
